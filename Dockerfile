FROM golang:1.20.3-alpine3.16

RUN mkdir /app
ADD . /app
WORKDIR /app
RUN go build -o main2 .
CMD ["/app/main2"]